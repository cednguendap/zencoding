<?php
//header("Location: Applications/Frontend/frontend.php");
require './Library/Classes/Autoload.class.php';

//preg_match('/admin\/([a-z]+)/i', $_SERVER['REQUEST_URI'],$infos)
if(strpos( $_SERVER['REQUEST_URI'], 'admin')>-1)
{
	$app=new \Applications\Backend\Classes\BackendApplication();
	$app->run();
}
else
{
	$app=new \Applications\Frontend\Classes\FrontendApplication();
	$app->run();
}
?>