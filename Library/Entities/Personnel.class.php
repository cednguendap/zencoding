<?php
namespace Library\Entities;


class Personnel extends \Library\Classes\Entity
{
	protected $Nom;
	protected $Prenom;
	protected $Login;
	protected $Password;
	protected $droit;
	protected $Email;
	protected $Sexe;
	protected $telephone;
	protected $Pseudo;
	
	public function __construct(array $donnees=array())
	{
		parent::__construct($donnees,"Personnel");	
	}

	
	public function getPseudo()
	{
		return $this->Pseudo;
	}
	
	public function getNom()
	{
		return $this->Nom;
	}	
	public function getPrenom()
	{
		return $this->Prenom;
	}	
	public function getLogin()
	{
		return $this->Login;
	}	
	public function getPassword()
	{
		return $this->Password;
	}
	public function getDroit()
	{
		return $this->droit;
	}	
	public function getEmail()
	{
		return $this->Email;
	}
	public function getSexe()
	{
		return $this->Sexe;
	}
	public function getTelephone()
	{
		return $this->telephone;
	}
	
	public function setNom($Nom)
	{
		if(is_string($Nom))
		{
			$this->Nom=$Nom;
		}
	}
	public function setPseudo($Pseudo)
	{
		if(is_string($Pseudo))
		{
			$this->Pseudo=$Pseudo;
		}
	}
	public function setPrenom($Prenom)
	{
		if(is_string($Prenom))
		{
			$this->Prenom=$Prenom;
		}
	}
	public function setLogin($Login)
	{
		if(is_string($Login))
		{
			$this->Login=$Login;
		}
	}
	public function setPassword($Password)
	{
		if(is_string($Password))
		{
			$this->Password=$Password;	
		}
	}
	public function setDroit($Droit)
	{
		//echo "La droit".$Droit;
		if(is_numeric($Droit))
		{
			$this->droit=$droit;
		}
	}
	public function setEmail($Email)
	{
		if(is_string($Email))
		{
			$this->Email=$Email;
		}
	}
	public function setSexe($Sexe)
	{
		if(is_string($Sexe))
		{
			$this->Sexe=$Sexe;
		}
	}
	public function setTelephone($tel)
	{
		if(is_numeric($tel))
			$this->telephone=$tel;
	}
}

?>