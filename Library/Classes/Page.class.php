<?php
namespace Library\Classes;


class Page extends ApplicationComponent
{
	protected $contentFile;
	protected $File=null;
	protected $vars=array();
        protected $error=Array();
        protected $contentHeader='';
        protected $contentFooter='';
        const TEMPLATE_LOCATION=1;
        const MODULE_LOCATION=2;
        const LIBRARY_LOCATION=3;
        const INVALID_ARGUMENT=1;
        const ONKNOW_VIEW=2;

        public function __construct($app)
	{
		parent::__construct($app);
	}
	public function addVar($var,$value)
	{
		if(!is_string($var) || is_numeric($var) || empty($var))
		{
			throw new \InvvalidArgumentException('Le nom des variables doivent être un chaine de carracterre valide',self::INVALID_ARGUMENT);
		}
		$this->vars[$var]=$value;
	}
	public function getGeneratedPage()
	{
		if(!file_exists($this->File) && count($this->error)==0)
		{
			throw new \RuntimeException('La vue specifique n\'existe pas',self::ONKNOW_VIEW);
		}
		extract($this->vars);
                $listError=$this->error;
		if(count($this->error)!=0)
                {
                   $this->File=__DIR__.'/../../Applications/'.$this->app->name().'/Erreurs/handle_error.php';
                }
                 ob_start();
                 require $this->File;
                 $this->contentFile=ob_get_clean();
		ob_start();
		require __DIR__.'/../../Applications/'.$this->app->name().'/Template/layout.php'; //a Le layout*/
		return ob_get_clean();
	}
        public function addError(Exception $e)
        {
            $this->error[]=$e;
        }
        public function addErrorException(\ErrorException $e)
        {
            $this->error[]=$e;
        }
        public function setContentFile($contentFile)
	{
		
		if(!is_string($contentFile) || empty($contentFile))
		{
                    throw new \InvalidArgumentException('La vue doit etre specefié',self::ONKNOW_VIEW);
		}
		$this->File=$contentFile;
	}
	public function NomSite()
	{
		return $this->app->Configuration()->get('Nom_du_site');
	}
	public function getContent()
	{
		try
                {
                    if(substr($this->File, strrpos($this->File, '.'))=='.php')
			return $this->contentFile;
                    else
                            return file_get_contents($this->File);
                } catch (\Exception $ex) 
                {
                    throw new \RuntimeException("Le fichier ".$this->File." n'existe pas ou a été déplacé",self::ONKNOW_VIEW);
                }
                catch (\ErrorExceptionrorException $ex) 
                {
                    throw new \RuntimeException("Le fichier ".$this->File." n'existe pas ou a été déplacé",self::ONKNOW_VIEW);
                }
	}
	public function getUser()
	{
		return $this->app->User();
	}
        public function url_for($res)
        {
            if(file_exists('Applications/'.$this->app->name().'/Template/'.$res))
                    return 'Applications/'.$this->app->name().'/Template/'.$res;
            throw new \RuntimeException("Le ficher ".$res." n'existe pas dans le dossier template",self::ONKNOW_VIEW);
            
        }
        public function url_template_for($res)
        {
            return $this->url_for($res);
        }
        public function url_library_for($res)
        {
            if(file_exists('Library/Classes/'.$res))
                return 'Library/Classes/'.$res;
            throw new \RuntimeException("Le ficher ".$res." n'existe pas dans la library",self::ONKNOW_VIEW);
        }
        private function url_module_for($type,$res)
        {
            if(file_exists('Applications/'.$this->app->name().'/Modules/'.$this->app->controller()->getModule().'/Views/'.ucfirst($type).'/'.$res))
                return 'Applications/'.$this->app->name().'/Modules/'.$this->app->controller()->getModule().'/Views/'.ucfirst($type).'/'.$res;
            throw new \RuntimeException("Le ficher ".$res." n'existe pas dans le module ".$this->app->controller()->getModule(),self::ONKNOW_VIEW);
             
        }
        public function header()
        {
            return $this->contentHeader;
        }
        public function addSpecificContent($type,$value,$location=TEMPLATE_LOCATION)
        {
            if($location==self::TEMPLATE_LOCATION)
            {
                if($type=='css') $this->contentHeader.='<link rel="stylesheet" href="'.$this->url_template_for($value).'">';
                else $this->contentFooter.='<script src="'.$this->url_template_for($value).'"></script>';
            }
            else if($location==self::MODULE_LOCATION)
            {
                if($type=='css') $this->contentHeader.='<link rel="stylesheet" href="'.$this->url_module_for($type,$value).'">';
                else $this->contentFooter.='<script src="'.$this->url_module_for($type,$value).'"></script>';
            } 
            else if($location==self::LIBRARY_LOCATION)
            {
                if($type=='css') $this->contentHeader.='<link rel="stylesheet" href="'.$this->url_library_for($value).'">';
                else $this->contentFooter.='<script src="'.$this->url_library_for($value).'"></script>';
            }
            else
                throw new \InvalidArgumentException("La localisation du fichier demandé doit être connues",self::ONKNOW_VIEW);
        }
        
        public function footer()
        {
            return $this->contentFooter;
        }
}
?>