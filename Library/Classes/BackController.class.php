<?php
namespace Library\Classes;

class BackController extends ApplicationComponent
{
	protected $action='';
	protected $module='';
	protected $page=null;
	protected $view='';
	protected $managers;

	public function __construct($app, $module,$action)
	{
		parent::__construct($app);
		$this->page=new \Library\Classes\Page($app);
                $this->dao=PDOFactory::getMysqlConnexion($app);
		$this->managers=new \Library\Classes\Managers('PDO',$this->dao);
		$this->setModule($module);
		$this->setAction($action);
	}
	public function execute()
	{
		$method='execute'.ucfirst($this->action);
		if(!is_callable(array($this,$method)))
		{
			throw new \RuntimeException('L\'action "'.$this->action.'" n\'est pas definis dans ce module');
		}
                try
                {
                    $this->dao->beginTransaction();
                    $this->$method($this->app->httpRequest());
                    $this->dao->commit();
                } catch (Exception $ex) {
                    $dbh->dao->roolBack();
                    throw $e;
                }
		
	}
	public function page()
	{
		return $this->page;
	}
	public function setModule($module)
	{
		$this->module=$module;
	}
	public function setAction($action)
	{
		$this->action=$action;
	}
        public function getModule()
        {
            return $this->module;
        }

        public function setView($view)
	{
		if(!is_string($view) || empty($view))
		{
			throw new \InvalidArgumentException('La vue doit etre une chaine de carractere valide');
		}
		$this->view=$view;
		$this->page->setContentFile(__DIR__.'/../../Applications/'.$this->app->name().'/Modules/'.$this->module.'/Views/Web/'.$this->view);
	}

}
?>