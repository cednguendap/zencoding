<?php
namespace Library\Classes;

class Entity implements \ArrayAccess
{
	protected $erreurs=array();
	protected $id;
	protected $name;
        protected $len;
        const INVALID_ARGUMENT=1;

	public function __construct(array $donnees=array(),$name)
	{
		$this->setName($name);
		if(!empty($donnees))
		{
			$this->hydrate($donnees);
		}
	}
        public function setLen($len)
        {
            if(!is_numeric($len))
            {
               throw new \InvalidArgumentException("L'argument name de la fonction setLen de valeur: ".$len." doit être un entier",self::INVALID_ARGUMENT); 
            }
            $this->len=$len;
        }
	public function setName($name)
	{
		if(!is_string($name))
		{
                    throw new \InvalidArgumentException("L'argument name de la fonction setName de valeur: ".$name." doit être une chaine de carractere",self::INVALID_ARGUMENT);
		}
                $this->name=$name;
	}
	public function getName()
	{
		return $this->name;
	}
	public function isNew()
	{
		return empty($this->id);
	}

	public function erreurs()
	{
		return $this->erreurs;
	}
	public function id()
	{
		return $this->id;
	}
        public function len()
        {
            return $this->len;
        }
	public function setId($id)
	{
		$this->id=$id;
	}
	public function hydrate(array $donnees)
	{
		foreach ($donnees as $attribut => $valeur)
		{
			$method='set'.ucfirst($attribut);

			if(!is_callable(array($this,$method)))
			{
                             throw new \InvalidArgumentException("Erreur de cles de tableau '".$var."'  methode d'entité '".$method."' inexistante ",self::INVALID_ARGUMENT);
			}
                        $this->$method($valeur);
                        
		}
	}
	public function offsetGet($var)
	{
		$method = 'get'.ucfirst($var);
		if(!isset($this->$var) || !is_callable(array($this,$method)))
		{
                    throw new \InvalidArgumentException("Erreur de cles de tableau '".$var."'  methode d'entité '".$method."' inexistante ",self::INVALID_ARGUMENT);
                }
                return $this->$var();
	}
	public function offsetSet($var, $value)
  	{
	    $method = 'set'.ucfirst($var);
	    
	    if (!isset($this->$var) || !is_callable(array($this, $method)))
	    {
                throw new \InvalidArgumentException("Methode innexistante pour l'argument : ".$var,self::INVALID_ARGUMENT);
	    }
            $this->$method($value);
  	}
  
  	public function offsetExists($var)
        {
    	return isset($this->$var) && is_callable(array($this, $var));
  	}
  
  	public function offsetUnset($var)
  	{
    	throw new \InvalidArgumentException('Impossible de supprimer une quelconque valeur',self::INVALID_ARGUMENT);
  	}
  	public function toArray()
  	{
  		$tab=array();
  		foreach ($this as $key => $value) {
  			$tab[$key]=$value;
  		}
  		return $tab;
  	}
}
?>