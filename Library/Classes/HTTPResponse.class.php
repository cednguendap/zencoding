<?php
namespace Library\Classes;


class HTTPResponse extends ApplicationComponent
{
	protected $page;
	protected $message;

	public function __construct($app)
	{
		parent::__construct($app);
	}

	public function addHeader($header)
	{
		header($header);
	}
	public function redirect($location)
	{
		header('Location: '.$location);
		exit;
	}
	public function redirect404()
	{
		echo "404";
		$this->page=new Page($this->app);
		$this->page->addVar('title','404');
		$this->page->setContentFile(__DIR__.'/../../Applications/'.$this->app->name().'/Erreurs/404.html');
		$this->addHeader('HTTP/1.0 404 Not Found');
		$this->send();
	}
	public function send()
	{
		if(empty($this->message))
		{
			exit($this->page->getGeneratedPage());
		}
		else
		{
			$this->sendMessage($this->message);
		}
		
	}
	public function setCookie($name,$value='',$expire=0,$path=null,$domain=null,$secure=false,$httpOnly=true)
	{
		setcookie($name,$value,$expire,$path,$domain,$secure,$httpOnly);
	}
	public function setPage(Page $page)
	{
		$this->page=$page;
	}
	public function sendMessage($message)
	{
		echo $message;
	}
	public function setMessage($message)
	{
		$this->message=$message;
	}
}
?>