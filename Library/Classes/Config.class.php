<?php
namespace Library\Classes;

class Config extends ApplicationComponent
{
	protected $vars=array();


	public function __construct($app)
	{
		parent::__construct($app);
	}
	public function get($vars)
	{
		if(!$this->vars)
		{
			$xml=new \DOMDocument;
			$xml->load(__DIR__.'/../../Applications/'.$this->app->name().'/Config/app.xml');
			$elements=$xml->getElementsByTagName('define');
			foreach ($elements as $element)
			{
				$this->vars[$element->getAttribute('var')]=$element->getAttribute('value');
			}

		}
		if(isset($this->vars[$vars]))
		{
			return $this->vars[$vars];
		}
		return null;
	}
}

?>