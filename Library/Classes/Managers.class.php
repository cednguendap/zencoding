<?php
namespace Library\Classes;


class Managers extends Manager
{
	protected $api=null;
	protected $managers=array();
	
	public function __construct($api,$dao)
	{
		parent::__construct($dao);
		$this->api=$api;
	}

	public function getManagerOf($module)
	{
		if (!is_string($module) || empty($module)) {
			throw new \InvalidArgumentException('Le module specifier est invalid');
		}
		if(!isset($this->managers[$module]))
		{
			$manager='\\Library\\Models\\'.ucfirst($module).'\\'.$this->api.'_Manager';
			$this->managers[$module]=new $manager($this->dao);
		}
		return $this->managers[$module];
	}
        public function getManagerOfSQL()
        {
            if(!isset($this->managers['SQL']))
            {
               $this->managers['SQL']=new \Library\Classes\Sql($this->dao);
            }
            return $this->managers['SQL'];
        }
}

?>