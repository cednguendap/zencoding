<?php
/*

*/
namespace Library\Classes;

abstract class Application
{
	protected $httpRequest;
	protected $httpResponse;
	protected $name;
	protected $user;
	protected $Config;
        protected $exception;
        protected $securite;
        
        function __construct()
	{
            $this->exception=new Exception($this);
            //set_exception_handler([$this->exception,'']);
            set_error_handler("Library\Classes\Exception::error2Exception");
            $this->httpRequest=new HTTPRequest($this);
            $this->httpResponse=new HTTPResponse($this);
            $this->securite=new Securite($this);
            $this->name='';	
            $this->user=new User;
            $this->Config=new Config($this);
            $this->Controller=null;
	}
	abstract public function run();
	public function getControlleur()
	{
            $router=new \Library\Classes\Router;
            $xml=new \DOMDocument;
            $xml->load(__DIR__.'/../../Applications/'.$this->name.'/Config/routes.xml');
            $routes=$xml->getElementsByTagName('route');
            foreach ($routes as $route)
            {
                    $var=array();
                    if($route->hasAttribute('vars'))
                    {
                            $vars=explode(',', $route->getAttribute('vars'));	
                    }
                    $router->addRoute(new Route($route->getAttribute('url'),$route->getAttribute('modules'),$route->getAttribute('action'),$var));
            }
            try
            {
                    $matchedRoute=$router->getRoute($this->httpRequest->requestURI());
            }
            catch (\RuntimeException $e)
            {
                    //echo $this->httpRequest->requestURI();
                
                    if($e->getCode()== Router::NO_ROUTE)
                            $this->httpResponse->redirect404();
                    else throw $e;
            }
            $_GET=array_merge($_GET,$matchedRoute->vars());

            $controllerClass='Applications\\'.$this->name.'\\Modules\\'.$matchedRoute->module().'\Controller\\'.$matchedRoute->module().'Controller';
            $this->Controller=new $controllerClass($this,$matchedRoute->module(),$matchedRoute->action());
            return $this->Controller;

	}
	public function httpRequest()
	{
		return $this->httpRequest;
	}
        public function controller()
        {
            return $this->Controller;
        }
	public function httpResponse()
	{
		return $this->httpResponse;
	}
	public function name()
	{
		return $this->name;
	}
	public function Configuration()
	{
		return $this->Config;
	}
	public function User()
	{
		return $this->user;
	}
}
?>