/******* 
Auteur: Nguendap Bedjama Cedric
Date de creation: 10/6/2018
Date de derniere modification: 17/10/2018

Fonction:Cet fichier permet l'accés simplifier a l'ajax; 
	Il contient 3 classes dont :
	>La classe Requete qui est la classe mere
	>La classe getRequete qui permet les requetes de type get
	>La classe postRequete qui permet les requetes de  type post

Dans la methode Run()
	L'objet formulaire doit être le premier element
	A partir du second element on regarde si c'est un tableau, un objet 
	au format JSON ou une liste d'élément

*******/

function Requete()
{
	var XHR;
	var serveur;
	var login='';
	var password='';
	var mode=true;
	var method;
	if(typeof Requete.initialized=='undefined')
	{
		Requete.prototype.init=function(UrlServeur,loginServeur,MdpServeur,modeServeur)
		{
			//Doit verifier si sa connait la requette de type XmlHttpRquest
			if(window.XMLHttpRequest || window.ActiveXObject)
			{
				if(window.ActiveXObject)
				{
					try{
						this.XHR=new ActiveXObject("Msxml2.XMLHTTP");
					} catch(e){
						this.XHR=new ActiveXObject("Microsoft.XMLHTTP");
					}
				}
				else
				{
					this.XHR=new XMLHttpRequest();
				}
				this.serveur=UrlServeur;
				this.login=loginServeur || "";
				this.password=MdpServeur || "";
				this.mode=modeServeur || true ;
			}
			else
			{
				this.XHR=null;
				//doit lancer une exception du a la non aceptation de l'object
				//XHR
			}
		};		
		
		Requete.prototype.exitRequest=function()
		{
			this.XHR.abort();
		};
		Requete.prototype.status=function()
		{
			//Pour le status de la requette
			return this.XHR.status;
		};
		Requete.prototype.Header=function(elmt)
		{
			return this.XHR.getResponseHeader(elmt);
		};
		Requete.prototype.receiv=function(callBAckFunction)
		{
			if(mode=true)
			{
				this.XHR.onreadystatechange=function(e)
				{
					var xhr=e.currentTarget;
					if(xhr.readyState==xhr.DONE && xhr.status==200)
					{ 

						var ResponseType=e.currentTarget.getResponseHeader('Content-Type');
						if(ResponseType.indexOf('xml')>-1)
						{
							callBAckFunction(xhr.responseXML);
						}
						else if(ResponseType.indexOf('json')>-1)
						{
							console.log(xhr.responseText);
							callBAckFunction(JSON.parse(xhr.responseText));
						}
						else
						{
							callBAckFunction(xhr.responseText);
						}
					}
				}
			}
			else
			{
				var ResponseType=this.Header('Content-Type');
				if(ResponseType.indexOf('XML')>-1)
				{
					callBAckFunction(this.XHR.responseXML);
				}
				else if(ResponseType.indexOf('JSON')>-1)
				{
					callBAckFunction(JSON.parse(this.XHR.responseText));
				}
				else
				{
					callBAckFunction(this.XHR.responseText);
				}	
			}
		};
		Requete.prototype.AllHeader=function()
		{
			return this.XHR.getAllResponseHeaders();
		};
		Requete.prototype.setHeaderJsonData = function() {
			this.XHR.setRequestHeader("Content-Type","application/json");
		};
		Requete.prototype.progress = function(functionParam) {
			this.XHR.onprogress=function(e)
			{
				functionParam(e.loaded,e.total);
			}
		};		
		
	};
	Requete.initialized=true;
}

function postRequete()
{
	Requete.call(this);
	this.Oform=null;
	if(typeof postRequete.initialized=='undefined')
	{
		for (var elmt in Requete.prototype) {
			postRequete.prototype[elmt]=Requete.prototype[elmt];
		}
		postRequete.prototype.run=function()
		{
			
			this.XHR.open('POST',this.serveur,this.mode,this.login,this.password);
			this.method='POST';
			if(arguments.length>0 && typeof arguments[0].nodeName!='undefined' && arguments[0].nodeName.toLowerCase()=='form')
			{
				this.Oform=new FormData(arguments[0]);
			}
			else
			{
				/*his.setHeaderJsonData();
				this.Oform=;
				console.log(this.Oform);*/
				//console.log();
				this.Oform=new FormData();
				this.Oform.append("argv",JSON.stringify(arguments[0]));
			}
			/*for (var i = 0; i < arguments.length; i++) {
				if(arguments[i] instanceof Array)
				{
					for (var j = 0; j < arguments[i].length; i++) {
						this.Oform.append(j,arguments[i][j]);
					}
				}
				else if(typeof(arguments[i])=='object' && typeof arguments[i].nodeName=='undefined')
				{
					//&& arguments[i].nodeName.toLowerCase()!='form'
					for(var elmt in arguments[i])
					{
						this.Oform.append(elmt,arguments[i][elmt]);
					}
				}
				else if(arguments[i] instanceof String)
				{
					this.Oform.append(i,arguments[i]);
				}
			}*/
		};
		postRequete.prototype.send= function(callBAckFunction) {
			//Pour le post	
			this.XHR.send(this.Oform);
			this.receiv(callBAckFunction);
		};
		postRequete.prototype.append = function(cles,valeur)
		{
			this.Oform.append(cles,valeur);
		};

	}
	postRequete.initialized=true;
}

function getRequete()
{
	Requete.call(this);
	if(typeof getRequete.initialized=='undefined')
	{
		for (var elmt in Requete.prototype) {
			getRequete.prototype[elmt]=Requete.prototype[elmt];
		}
		getRequete.prototype.run=function (Param) {
			this.method='GET';
			var ListeParam='';
			if(Param instanceof Array)
			{
				for (var j = 0; j < Param.length-1; i++) {
					ListeParam=ListeParam+j+'='+encodeURIComponent(Param[j])+'&';
				}
				ListeParam=ListeParam+encodeURIComponent(Param[j+1]);
			}
			else if(typeof(Param)=='object')
			{
				for(var elmt in Param)
				{
					ListeParam=ListeParam+elmt+'='+encodeURIComponent(Param[elmt])+'&';
				}
				ListeParam=ListeParam.substr(0,ListeParam.lastIndexOf('&'));
			}
			else
			{
				ListeParam=j+'='+encodeURIComponent(Param);
			}
			this.XHR.open('GET',this.serveur+'?'+ListeParam,this.mode,this.login,this.password);
		};
		getRequete.prototype.send= function(callBAckFunction)
		{
			this.XHR.send(null);
			this.receiv(callBAckFunction);
		};

	}
	getRequete.initialized=true;
}