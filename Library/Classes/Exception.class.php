<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Library\Classes;

/**
 * Description of Exception
 * Cette classe est une surper classe pour gestion des erreurs et des exceptions dans toute l'application 
 *
 * @author Ppitbull
 */
class Exception extends ApplicationComponent 
{
    protected  $debug_mode=true;
    protected $page;
    public function __construct($app/*,Page $page*/,$debug=true)
    {
        parent::__construct($app);
        //$this->page=page;
        $this->debug_mode=$debug;
        $this->config_debug_mode();
    }
    protected function config_debug_mode()
    {
        //si on veut gerer les erreur
        //set_error_handler($this,'gestion_erreur');
        if($this->debug_mode)
        {
            error_reporting(E_ALL|E_STRICT);
        }
        else
        {
            error_reporting(~E_ALL);
        }
    }
    public function gestion_exception(Exception $e)
    {
        
    }
    protected function gestion_erreur($niveau,$message,$fichier,$ligne)
    {
        if($niveau && error_reporting() )
        {
            //on prepare le message a affiché
            if($niveau==E_USER_ERROR) exit (255);
        }
    }
    public static function error2Exception($niveau,$message,$fichier,$ligne)
    {
        if($niveau==E_USER_ERROR || $niveau==E_ERROR || $niveau==E_CORE_ERROR)
        {
            throw new \RuntimeException($message,2,$fichier, $ligne);
        }
        throw new \ErrorException($message, 0, 1, $fichier, $ligne);
    }
    
}
