/***Cette page prevoit des fonction de 
compatibilité avec tous les navigateur***/

function getElement(id)
{
	if (document.querySelector('#'+id)!=null)
	{
		return document.querySelector('#'+id);	
	}
	else
	{
		return document.querySelector(id);
	}
	
}
function getElements(id)
{
	if (document.querySelectorAll('#'+id)!=null)
	{
		return document.querySelectorAll('#'+id);	
	}
	else
	{
		return document.querySelectorAll(id);
	}
	
}
function positionElemt(elmt)
{
	var X=0;
	var Y=0;
	var elmt2=elmt;
	while(elmt.parentNode.nodeName.toLowerCase()!="body")
	{
		console.log(elmt);
		console.log("X "+X+" Y "+Y);
		X+=getStyle(elmt).marginRight;
		Y+=getStyle(elmt).marginTop;
		elmt=elmt.parentNode;
	}
	return {'X':X,'Y':Y};
}
function TagContent(elmt,text)
{
	if (window.innerText) {
		elmt.innerText=text;
	}
	else
	{
		elmt.textContent=text;

	}
}
function getTagContent(elmt)
{
	if (window.innerText) {
		return elmt.innerText;
	}
	else
	{
		return elmt.textContent;

	}
}

function convertMethod(obj,method,infos)
{
	return function(event)
	{
		method.call(obj,event||window.event,infos);
	}
}
function contextMethod(obj,method,infos)
{
	method.call(obj,infos1,infos2,infos3);
}
function addEvent(elmt,callBackFunction,BoutEvent,obj,infos,propage)
{
	if(arguments.length>=4)
	{
		propage=propage|| false;
		var fonct=convertMethod(obj,callBackFunction,infos);
		if(elmt.addEventListener)
		{
			elmt.addEventListener(BoutEvent,fonct,propage);
		}
		else
		{
			elmt.attachEvent('on'+BoutEvent,fonct);
		}
	}
	else
	{
		if(elmt.addEventListener)
		{
			elmt.addEventListener(BoutEvent,callBackFunction,false);
		}
		else
		{
			elmt.attachEvent('on'+BoutEvent,function(Event){
				callBackFunction(window.event);
			});
		}
	}
}

function removeEvent(elmt,callBackFunction,BoutEvent,obj)
{
	if(arguments.length>=4)
	{
		if(elmt.removeEventListener)
		{
			elmt.removeEventListener(BoutEvent,callBackFunction.call(obj),true);
		}
		else
		{
			elmt.detachEvent('on'+BoutEvent,callBackFunction.call(obj));
			
		}

	}
	else
	{
		if(elmt.removeEventListener)
		{
			elmt.removeEventListener(BoutEvent,callBackFunction(Event),true);
		}
		else
		{
			elmt.detachEvent('on'+BoutEvent,function(Event){
				callBackFunction(window.event);
			});
		}
	}
}

function getStyle(elmt)
{
	try
	{
		return getComputedStyle(elmt,null);
	}
	catch(e)
	{
		return elmt.currentStyle;
	}
}
function dragAndDrop(elmt)
{
	if(getElement(elmt.getId()).draggable==true)
	{
		addEvent(getElement(elmt.idTitleBar), function(Event)
		{
			elmt.statusMove=true;
			if(!(elmt instanceof WordJs))
			{
				Event.returnValue=false;
				if (Event.preventDefault) {
					Event.preventDefault();
				}
			}
		},'mousedown');
		addEvent(document,function(e)
		{
			if(elmt.statusMove)
			{
				/*var elmtX=0;
				var elmtY=0;
				var tag2=getElement(elmt.id);
				do{
					elmtX+=tag2.offsetLeft;
					elmtY+=tag2.offsetTop;
					tag2=tag2.parentNode;
					console.log("la");
					console.log(elmtX+" "+elmtY);
					console.log("lo");
				}while(tag2.id=="container");

				var SourisX=Event.clientY;
				var SourisY=Event.clientX;
				var dX=SourisX-elmtX;
				var dY=SourisY-elmtY;*/

				var SourisX=e.clientY;
				var SourisY=e.clientX;
				//console.log(SourisX+" "+SourisY);
				var tag=getElement(elmt.id);
				tag.style.top=SourisY+'px';
				tag.style.left=SourisX+'px';
				elmt.setPosition(SourisX,SourisY);
			}

		},'mousemove');
		addEvent(document,function(Event)
		{
			elmt.statusMove=false;
		},'mouseup');
	}
}

function search(tabSearch,value)
{
	if(typeof tabSearch.length=="undefined" )
	{
		for(var elmt in tabSearch)
		{
			if(elmt==value)
			{
				return true;
			}
		}
		return false;
	}
	else
	{
		for (var i = 0; i < tabSearch.length; i++) {
			if(tabSearch[i]==value)
			{
				return true;
			}
		}
		
		return false;
	}
}

function randomInt(min,max)
{
	var result=(max-min+1)*Math.random();
	result+=min;
	return Math.floor(result);
}

function Selection(elmt,callBackFunction)
{
	if(window.ActiveXObject)
	{
		var textRange=document.selection.createRange();
		var currentSelection=textRange.text;
		textRange.text=callBackFunction(currentSelection);
		textRange.select();
	}
	else
	{
		var startSelection=elmt.value.substring(0,elmt.selectionStart);
		var currentSelection=elmt.value.substring(elmt.selectionStart,elmt.selectionEnd);
		var endSelection=elmt.value.substring(elmt.selectionEnd);
		elmt.value=startSelection+callBackFunction(currentSelection)+endSelection;
	}
}