<?php
namespace Library\Classes;

class PDOFactory
{

	public static function getMysqlConnexion($app)
	{
		$db= new \PDO('mysql:host='.$app->Configuration()->get('host').';dbname='.$app->Configuration()->get('BD_Name'),$app->Configuration()->get('LoginBD'),$app->Configuration()->get('PassBD'));
		$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		return $db;
	}
}
?>