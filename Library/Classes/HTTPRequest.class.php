<?php
namespace Library\Classes;

class HTTPRequest extends ApplicationComponent
{
	public function __construct($app)
	{
		parent::__construct($app);
	}
	public function cookieData($key)
	{
		return isset($_COOKIE[$key]) ? $_COOKIE[$key]:null;
	}
	public function cookieExist($key)
	{
		return isset($_COOKIE[$key]);
	}
	public function getData($key)
	{
		return isset($_GET[$key]) ? $this->valideData($_GET[$key]):null;
	}
	public function getExist($key)
	{
		return isset($_GET[$key]);
	}
	public function method()
	{
		return $_SERVER['REQUEST_METHOD'];
	}
	public function postData($key)
	{
		return isset($_POST['argv'])? (isset(json_decode($_POST['argv'],true)[$key])? $this->valideData(json_decode($_POST['argv'],true)[$key]):null) : (isset($_POST[$key]) ? $this->valideData($_POST[$key]):null);
	}
	public function postInvalidParam($key)
	{
		return isset($_POST['argv'])? (isset(json_decode($_POST['argv'],true)[$key])? json_decode($_POST['argv'],true)[$key]:null) : (isset($_POST[$key]) ? $_POST[$key]:null);
	}
	public function postExist($key)
	{
		return isset($_POST[$key]);
	}
	public function requestURI()
	{
		return $_SERVER['REQUEST_URI'];
	}
	public function valideData($data)
	{
		return strip_tags($data);
	}
	
	public function fileData($key)
	{
		return isset($_FILES[$key])?$_FILES[$key]:null;
	}
	public function fileArray()
	{
		return isset($_FILES)? $_FILES:null;
	}
}
?>