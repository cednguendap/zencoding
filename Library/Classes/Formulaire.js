/***Cette classe permet la manipulation simple des formulaires***/

function Formulaire()
{
	var id;
	var List=null;
	var ListForSend=null;
	if(typeof Formulaire.initialized=='undefined')
	{
		Formulaire.prototype.init = function(id) {
			var form=document.querySelector("#"+id);
			var nouedFils=form.childNodes;
			for (var i = 0; i < nouedFils.length; i++) {
				var fils=Things[i];
				if(fils.nodeName.toLowerCase()=='input')
					this.List[fils.id]={};
			}
		};
		Formulaire.prototype.isValide = function() {
			for(elmt in this.List)
			{
				var obj=document.querySelector('#'+elmt);
				var value=obj.value;
				if(obj.type=='email')
				{
					if(/^[a-zA-Z0-9_-]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]{2,6}$/.test(value))
						this.List[elmt]['valide']=true;
					else
						this.List[elmt]['valide']=false;
					this.List[elmt]['type']='email';
				}
				else if(obj.type=='text')
				{
					if(value.length>2)
					{
						this.List[elmt]['valide']=true;
					}
					else
						this.List[elmt]['valide']=false;
					this.List[elmt]['type']='text';
				}
				else if(obj.type=='date')
				{
					if(/^([0-9]{2,2})\/([0-9]{2,2})\/([0-9]{2,2})$/.test(value))
					{
						if($1>0 && $1<=30 && $2>0 && $2<12 && $3>0)
							this.List[elmt]['valide']=true;
						else
							this.List[elmt]['valide']=false;
						this.List[elmt]['type']='date';		
					}
				}
				else if(obj.type=='number')
				{
					if(value.length>0)
					{
						this.List[elmt]['valide']=true;
					}
				}
				else if(obj.type=='radio')
				{
					if(obj.isChecked)
					{
						this.List[elmt]['valide']=true;
						this.List[elmt]['type']='checkbox';
					}
				}
				else if(obj.type=='checkbox')
				{
					if(obj.isChecked)
					{
						this.List[elmt]['valide']=true;
						this.List[elmt]['type']='checkbox';
					}
				}
			}
			Formulaire.prototype.append = function(name,value) {
				if(name.length>0 && value.length>0)
					this.ListForSend[name]=value;
				this.List[elmt]['type']='autre';
			};
			Formulaire.prototype.getList = function() {
				if(this.List==null)
				{
					return null;
				}
				else
				{
					if(this.ListForSend==null)
					{						
						for(elmt in this.List)
						{
							if(this.List[elmt][valide])
							{	/*if(this.List[elmt]['type']=='checkbox' || this.List[elmt]['type']=='radio')
								{

								}*/
								var obj=document.querySelector('#'+elmt);
								this.ListForSend[obj.name]=obj.value;
							}
						}
					}
					return this.ListForSend;
				}

			};
		};
	}
	Formulaire.initialized=true;
}