<?php
namespace Library\Classes;

/**
* 
*/
class File
{
	const  LECTURE_SEULE_DEBUT="r";
	const  ECRITURE_SEULE_DEBUT="w";
	const  LECTURE_ECRITURE_DEBUT="w+";
	const  LECTURE_ECRITURE_FIN="a+";
	const  ECRITURE_SEULE_FIN="a";
	const  DEBUT_FICHIER=0;
	const  FIN_FICHIER=1;
	const  EXECUTABLE=0;
	const  FICHIER=1;
	const  REPERTOIRE=2;
	const  LINK=3;



	public static function insertion($url,$contenu,$position=File::DEBUT_FICHIER,$mode=File::ECRITURE_SEULE_FIN)
	{
			if($position==File::DEBUT_FICHIER)
			{
				file_put_contents($url, $contenu);
			}
			else if($position==File::FIN_FICHIER)
			{
				file_put_contents($url, $contenu,FILE_APPEND);		
			}
			else
			{
				$fichier=fopen($url, $mode);
				fseek($fichier, $position);
				fwrite($fichier, $contenu);
				fclose($fichier);	
			}
			return true;
		
	}
	public static function lecture($url,$positionDepart=File::DEBUT_FICHIER,$positionFin=File::FIN_FICHIER)
	{

		if (File::exist($url)) {
			if($positionDepart==File::DEBUT_FICHIER && $positionFin==File::FIN_FICHIER)
			{
				return file_get_contents($url);
			}
			else
			{
				$fichier=fopen($url, $mode);
				$chaine="";
				$pos=$positionDepart;
				while ($pos<=$positionFin && !File::finFichier($fichier)) {
					$chaine.=fread($fichier, 4090);
					$pos+=fseek($fichier, $pos);
				}
				fclose($fichier);
			}
		}
	}
	public static function exist($url)
	{
		return file_exists($url);
	}
	public static function fin($Descripteur)
	{
		return feof($Descripteur);
	} 
	public static function suppression($url)
	{
		if(File::exist($url))
		{
			return unlink($url);
		}
		else
		{
			return false;
		}
	}
	public static function type($url)
	{
		if(is_executable($url))
		{
			return File::EXECUTABLE;
		}
		else if(is_file($url))
		{
			return File::FICHIER;
		}
		else if(is_link($url))
		{
			return File::LINK;
		}
		else if(is_dir($url))
		{
			return File::REPERTOIRE;
		}
	}
	public static function nouveauDossier($url,$recursive=true)
	{
		return mkdir($url,0777,$recursive);
	}
	public static function renomeFichier($url,$nouveauNom)
	{
		return rename($url,$nouveauNom);
	}
	public static function deplacer($urlDepart,$urlFin)
	{
		copy($urlDepart, $urlFin);
	}
	public static function deplacerUploadedFichier($nomFichier,$urlFin)
	{
		return move_uploaded_file($nomFichier, $urlFin);
	}
	public static function DossierVide($urlDossier)
	{
		$dos=opendir($urlDossier);
		while (false!==($elmt=readdir($dos)))
		{
			if($elmt!='.' && $elmt!="..")
			{	
				return false;
			}
		}
		closedir($dos);
		return true;
	}
	public static function ListeDossier($urlDossier)
	{
		$dos=opendir($urlDossier);
		$List=array();
		while (false!==($elmt=readdir($dos)))
		{
			if($elmt!='.' && $elmt!="..")
			{	
				$List[]=$elmt;
			}
		}
		closedir($dos);
		return $List;
	}
	public static function uploadList($listFichier,$listNom,$urlDeport)
	{
		for ($i=0; $i <$listNom ; $i++) { 
			$fichier=$listFichier[$listNom[$i]];
			if($fichier['error']<=0)
			{
				File::upload($fichier,$urlDeport);
			}
		}
	}
	public static function upload($file,$path)
	{
		// Varibale d'erreur par soucis de lisibilité
		// Evite d'imbriquer trop de if/else, on pourrait aisément s'en passer
		$error = false;

		// On définis nos constantes
		$newName = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
		$legalExtensions= array("JPG", "PNG", "GIF","MP4","AVI","MKV");
		// On récupères les infos
		$actualName = $file['tmp_name'];
		$actualSize = $file['size'];
		$extension = pathinfo($file, PATHINFO_EXTENSION);

		// On s'assure que le fichier n'est pas vide
		if ($actualName == 0 || $actualSize == 0) {
		    $error = true;
		}

		// On vérifie qu'un fichier portant le même nom n'est pas présent sur le serveur
		if (file_exists($path.'/'.$newName.'.'.$extension)) {
		    $error = true;
		}
		
		// On effectue nos vérifications réglementaires
		if (!$error) {
		    if ($actualSize < $legalSize) {
		    	 
		        if (in_array($extension, $legalExtensions)) {
		            move_uploaded_file($actualName, $path.'/'.$newName.'.'.$extension);
		        }
		    }
		}

		else {
		    
		    // On supprime le fichier du serveur
		    @unlink($path.'/'.$newName.'.'.$extension);
		    
		    throw new RuntimeError("Impossible d'uploader le fichier de nom ".$actualName, 2);
		    
		    
		}
	}
	public static function suppressionDossier($urlDossier)
	{
		$dos=dir($urlDossier);
		while ($elmt=$dos->read())
		{
			if($elmt!='.' && $elmt!="..")
			{	
				$fichier=$urlDossier.'/'.$elmt;
				if(is_dir($fichier))
				{
					if (File::DossierVide($fichier))
					{
						rmdir($fichier);
					}
					else
					{
						File::suppressionDossier($fichier);
					}					
				}
				else
					unlink($fichier);
			}
		}
		$dos->close();
		rmdir($urlDossier);
	}
}

?>