<?php
namespace Library\Classes;

class Route extends ApplicationComponent
{
	protected $action;
	protected $module;
	protected $url;
	protected $varsNames;
	protected $vars=array();
	public function __construct($url,$module,$action,Array $varsNames)
	{
		parent::__construct($this);
		$this->setUrl($url);
		$this->setModule($module);
		$this->setAction($action);
		$this->setVarsNames($varsNames);
	}
	public function hasVars()
	{
		return !empty($this->varsNames);
	}
	public function match($url)
	{
		if(preg_match('`^'.$this->url.'$`',$url, $match))
		{
			return $match;
		}
		else
		{
			return false;
		}
	}
	public function setAction($action)
	{
		if(is_string($action))
		{
			$this->action=$action;
		}
	}
	public function setModule($module)
	{
            if(!is_string($module))
            {
                throw new \InvalidArgumentException("Le module doit être bien spécifier");
            }
            $this->module=$module;
	}
	public function setUrl($url)
	{
            if(!is_string($url))
            {
                throw new \InvalidArgumentException("L'url doit être bien spécifier");
            }
            $this->url=$url;
	}
	public function setVarsNames(Array $varsNames)
	{
		$this->varsNames=$varsNames;
	}
	public function setVars(Array $vars)
	{
		$this->vars=$vars;
	}
	public function action()
	{
		return $this->action;
	}
	public function module()
	{
		return $this->module;
	}
	public function url()
	{
		return $this->url;
	}
	public function varsNames()
	{
		return $this->varsNames;
	}
	public function vars()
	{
		return $this->vars;
	}
}
?>