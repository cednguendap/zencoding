<?php 
namespace Library\Classes;


class Router extends ApplicationComponent
{
	protected $routes=array();
	const NO_ROUTE=11;
	
	public function __construct()
	{
		parent::__construct($this);
	}
	public function addRoute(Route $route)
	{
		//echo "1 route";
		//if (!in_array($route, $this->routes)) {
			$this->routes[]=$route;
		//}

	}
	public function getRoute($url)
	{
		//echo $url;
		foreach ($this->routes as $route)
		{
			if(($varsValues=$route->match($url))!==false)
			{
				if($route->hasVars())
				{
					$varsNames=$route->varsNames();
					$listVars=array();
					foreach ($varsValues as $key => $match) 
					{
						if($key!==0)
						{
							$listVars[$varsNames[$key-1]]=$match;
						}
					}
					$route->setVars($listVars);
				}
				return $route;
			}
		}
		throw new \RuntimeException("aucune route ne correspond a cette url", self::NO_ROUTE);
		
	}

}
?>