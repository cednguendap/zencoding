<?php
namespace Library\Models\Personnel;

/**
* 
*/
class PDO_Manager extends Manager_Personnel
{
	
	public function addPersonnel(\Library\Entities\Personnel $perso)
	{
		$req=$this->dao->prepare('INSERT INTO 
		personnel (idPersonnel,Nom,Prenom,Sexe,Login,Password,Droit,Email) 
		VALUES(:idPersonnel,:nom,:prenom,:sexe,:login,:Password,:droit,:email)');
		$req->execute(array(
			'idPersonnel' =>$perso->getIdPersonnel(),
			'nom' =>$perso->getNom(),
			'prenom' =>$perso->getPrenom(),
			'login' =>$perso->getLogin(),
			'Password' =>$perso->getPassword(),
			'droit' =>$perso->getDroit(),
			'email' =>$perso->getEmail(),
			'sexe' =>$perso->getSexe()));

	}
	public function deletePersonnel(\Library\Entities\Personnel $perso)
	{
		$req=$this->dao->prepare('DELETE FROM personnel WHERE CNI=:cni');
		$req->execute(array('cni'=>$perso->getCNI()));
	}
	public function updatePersonnel(\Library\Entities\Personnel $perso)
	{
		$req=$this->dao->prepare('UPDATE personnel SET Nom=:nom,Prenom=:prenom,Login=:login,Password=:mdp,Droit=:droit,Email=:mail,Sexe=:sexe WHERE idPersonnel=:num');
		$req->bindValue(':cniRap', $perso->getId(), PDO::PARAM_INT);
		$req->bindValue(':nom', $perso->getNom(), PDO::PARAM_STR);
		$req->bindValue(':prenom', $perso->getPrenom(), PDO::PARAM_STR);
		$req->bindValue(':login', $perso->getLogin(), PDO::PARAM_STR);
		$req->bindValue(':mdp', $perso->getPassword(), PDO::PARAM_STR);
		$req->bindValue(':droit', $perso->getDroit(), PDO::PARAM_STR);
		$req->bindValue(':mail', $perso->getEmail(), PDO::PARAM_STR);
		$req->bindValue(':sexe', $perso->getSexe(), PDO::PARAM_STR);
		$req->execute();
	}
	public function updateNom(\Library\Entities\Personnel $perso)
	{
		$req=$this->dao->prepare('UPDATE personnel SET NOM=:nom WHERE idPersonnel=:num');
		$req->bindValue(':nom',$perso->getNom(),PDO::PARAM_STR);
		$req->bindValue(':num',$$perso->getId(),PDO::PARAM_STR);
		$req->execute();
		$req->closeCursor();	
	}
	public function updatePrenom(\Library\Entities\Personnel $perso)
	{
		$req=$this->dao->prepare('UPDATE personnel SET Prenom=:nom WHERE idPersonnel=:num');
		$req->bindValue(':nom',$perso->getPrenom(),PDO::PARAM_STR);
		$req->bindValue(':num',$$perso->getId(),PDO::PARAM_STR);
		$req->execute();
		$req->closeCursor();
	}
	public function selectPersonnel()
	{
		$req=$this->dao->query('SELECT * FROM personnel ');
		$req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE,'\Library\Entities\Personnel');
		$result=$req->fetchAll();
		$req->closeCursor();
		return $result;
		
	}
	public function selectPersonnelById(\Library\Entities\Personnel $perso)
	{
		$req=$this->dao->prepare('SELECT * FROM personnel WHERE idPersonnel=:id');
		$req->execute(array('id' => $perso->getIdPersonnel()));
		$req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE,'\Library\Entities\Personnel');
		$result=$req->fetchAll();
		$req->closeCursor();
		return $result;
	}
	public function authentif(\Library\Entities\Personnel $perso)
	{

		$req=$this->dao->prepare('SELECT * FROM personnel WHERE Password=:mdp AND Login=:login ');
			$req->execute(array('mdp'=>md5($perso->getPassword()),'login'=>$perso->getLogin()));
			$req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE,'\Library\Entities\Personnel');
			$result=$req->fetchAll();
			$req->closeCursor();
			
			return $result;		
	}
	public function authentifEmail(\Library\Entities\Personnel $perso)
	{
		$req=$this->dao->prepare('SELECT * FROM personnel WHERE Password=:mdp AND Email=:mail ');
			$req->execute(array('mdp'=>md5($perso->getPassword()),'mail'=>$perso->getEmail()));
			$req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE,'\Library\Entities\Personnel');
			$result=$req->fetchAll();
			$req->closeCursor();
			
			return $result;		
	}
	public function selectPersonnelByNom(\Library\Entities\Personnel $perso)
	{
		$req=$this->dao->prepare('SELECT idPersonnel FROM personnel WHERE Nom=:nom AND Prenom=:prenom');
		$req->execute(array('nom'=>$perso->getNom(),'prenom'=>$perso->getPrenom()));
		$req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE,'\Library\Entities\Personnel');

		$result=$req->fetchAll();

		$req->closeCursor();
		return $result;
	}
	public function selectPersonnelByEmail(\Library\Entities\Personnel $perso)
	{
		$req=$this->dao->prepare('SELECT * FROM personnel WHERE Email=:mail');
		$req->execute(array(':mail'=>$perso->getEmail()));
		$req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE,'\Library\Entities\Personnel');
		$result=$req->fetchAll();
		$req->closeCursor();
		return $result;
	}
	public function existMail(\Library\Entities\Personnel $perso)
	{
		$req=$this->dao->prepare('SELECT COUNT(*) AS nbre FROM personnel WHERE Email=:infos');
		$req->execute(array('infos'=> $perso->getEmail()));
		if(empty($req->fetch()))
		{
			$req->closeCursor();
			return false;
		}
		else
		{
			$req->closeCursor();
			return true;
		}
	}
	public function selectIdByName(\Library\Entities\Personnel $perso)
	{
		$req=$this->dao->prepare('SELECT idPersonnel FROM personnel WHERE Nom=:nom AND Prenom=:prenom');
		$req->execute(array('nom'=>$perso->getNom(),'prenom'=>$perso->getPrenom()));
		$req->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE,'\Library\Entities\Personnel');
		$result=$req->fetchAll();
		$req->closeCursor();
		return $result;
	}
	public function getElement($sql)
	{
		$req=$this->dao->query($sql,\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE,'\Library\Entities\Personnel');
		$result=$req->fetchAll();
		$req->closeCursor();
		return $result;		
	}

}
?>