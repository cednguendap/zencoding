<?php
namespace Library\Models\Personnel;

abstract class Manager_Personnel extends \Library\Classes\Manager
{
	abstract function addPersonnel(\Library\Entities\Personnel $perso);
	abstract function deletePersonnel(\Library\Entities\Personnel $perso);
	abstract function updatePersonnel(\Library\Entities\Personnel $perso);
	abstract function selectPersonnel();
}
?>
