<!DOCTYPE html>
<html lang="fr">

<head>
   <meta charset="utf-8">
	<title><?php echo $this->NomSite().'| '.$title; ?></title>
        <!--<base href="/" /> -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Bootstrap 3 template for corporate business" />
	<!-- css -->
        <link href="<?php echo $this->url_for('css/bootstrap.min.css')?>" rel="stylesheet" />
	<link href="<?php echo $this->url_for('plugins/flexslider/flexslider.css')?>" rel="stylesheet" media="screen" />
	<link href="<?php echo $this->url_for('css/cubeportfolio.min.css')?>" rel="stylesheet" />
	<link href="<?php echo $this->url_for('css/style.css')?>" rel="stylesheet" />
        <link href="<?php echo $this->url_for('bodybg/bg1.css')?>" rel="stylesheet" />
        <link href="<?php echo $this->url_for('css/custom.min.css')?>" rel="stylesheet">
	<!-- Theme skin -->
	<link id="t-colors" href="<?php echo $this->url_for('skins/default.css')?>" rel="stylesheet" />

	<!-- boxed bg -->
	<link id="bodybg" href="<?php echo $this->url_for('bodybg/bg1.css')?>" rel="stylesheet" type="text/css" />
        <?php echo $this->header(); ?>
        
</head>

<body>
	<div id="wrapper">
		<!-- start header -->
		<header>
			<!--<div class="top">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<ul class="topleft-info">
								<li>Contact:&nbsp;&nbsp;<i class="fa fa-phone"></i>  (+237) 652 142 126 - 656 476 819</li>
							</ul>
						</div>
						<div class="col-md-6">
							<div id="sb-search" class="sb-search">
								<form>
									<input class="sb-search-input" placeholder="Entrez le terme rechercher..." type="text" value="" name="search" id="search">
									<input class="sb-search-submit" type="submit" value="">
									<span class="sb-icon-search" title="Cliquez pour rechercher"></span>
								</form>
							</div>


						</div>
					</div>
				</div>
			</div>-->

			<div class="navbar navbar-default navbar-static-top">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
						<a class="navbar-brand" href="/isev/"><img src="<?php echo $this->url_for('img/logo.png')?>" alt="" width="199" height="52" /></a>
					</div>
					<div class="navbar-collapse collapse ">
						<ul class="nav navbar-nav">
							<li class="dropdown active">
								<a href="/" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Accueil <i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="/isev/actualites">Actualités</a></li>
									<li><a href="/publications">Publications</a></li>
									<li><a href="/ressources">Photos et vidéos</a></li>

								</ul>

							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Formations <i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="/formation/employer">Employé</a></li>
									<li><a href="/formation/entreprise">Entreprise</a></li>
								</ul>
							</li>
							
							<li class="dropdown"><a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Partenariat <i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="/partenariat/entreprises">Entreprise</a></li>
									<li><a href="/partenariat/hopital">Hôpital</a></li>
								</ul>
							</li>
							<li class="dropdown"><a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Santé <i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="/sante/education">Santé à l'éducation</a></li>
									<li><a href="/sante/soins">Accés aux soins</a></li>
								</ul>
							</li>
							<li class="dropdown"><a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Sensibilisation <i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="/sensibilisation/un-monde-plus-juste">Un monde plus juste</a></li>
									<li><a href="/sensibilisation/une-sante-de-fers">Une santé de fers</a></li>
								</ul>
							</li>
							<li class="dropdown"><a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Soutenez nous <i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="/soutient/dons">Faire un dons</a></li>
									<li><a href="/soutient/parrainez-projet">Parrainez un projet</a></li>
									<li><a href="/soutient/legs-heritages">Legs & héritages</a></li>
									<li><a href="/soutient/autres-soutient">Soutenez nous autrements</a></li>
									<li><a href="/soutient/zone-dons">Ou vont les dons</a></li>
								</ul>
							</li>
							<li class="dropdown"><a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Offres et services <i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="/offre-service/informatique">Informatique</a></li>
									<li><a href="/offre-service/fiscalite">Fiscalité</a></li>
									<li><a href="/offre-service/gescompt">Gestion comptable</a></li>
								</ul>
							</li>
							<li ><a href="#">A propos </a></li>
                                                        <?php if($this->getUser()->isAuthentificated()) { ?>
                                                        <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="Applications/Backend/Template/production/images/user.png" alt=""><?php echo $this->getUser()->Infos()->getNom()." ".$this->getUser()->Infos()->getPrenom() ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profil</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Parametre</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Aide</a></li>
                    <li><a href="/isev/admin/accueil">Administration</a></li>
                    <li><a href="/isev/admin/Logout"><i class="fa fa-sign-out pull-right"></i> Deconnexion</a></li>
                  </ul>
                </li>
                                                        <?php } ?>
						</ul>
					</div>
				</div>
			</div>
		</header>
		<!-- end header -->
		
                <section id="content">
                    <div class="row">
                        <div class="col-lg-12">
                           <?php echo $this->getContent(); ?>
                        </div>
                    </div>
                </div>
                </section>
                
			<!-- end divider -->
			
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 col-lg-3">
						<div class="widget">
							<h4>Contactez nous</h4>
							<address>
					<strong>Ideal Sante Et Vie</strong><br>
					 face Direction Générale HYSACAM,<br>
					 immeuble ancien maison CHANTALUX<br>
					  B.P. 5559 Douala </address>
							<p>
								<i class="icon-phone"></i> (+237) 652 142 126 / 656 476 819 <br>
								<i class="icon-envelope-alt"></i> lescliniquesidealesante@gmail.com
							</p>
						</div>
					</div>
					<div class="col-sm-3 col-lg-3">
						<div class="widget">
							<h4>Information</h4>
							<ul class="link-list">
								<li><a href="#">Actualité</a></li>
								<li><a href="#">Conditions d'utilisation</a></li>
								<li><a href="#">Charte ISEV</a></li>
								<li><a href="#">Contactez nous</a></li>
							</ul>
						</div>

					</div>
					<div class="col-sm-3 col-lg-3">
						<div class="widget">
							<h4>Pages</h4>
							<ul class="link-list">
								<li><a href="#">Acceuil</a></li>
								<li><a href="#">Actualités</a></li>
								<li><a href="#">Projets</a></li>
								<li><a href="#">Dons & legs</a></li>
								<li><a href="#">Dons</a></li>
								<li><a href="#">Photos et videos</a></li>
								<li><a href="#">Contactez nous</a></li>
								<li><a href="/isev/login">Connexion</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3 col-lg-3">
						<div class="widget">
							<h4>Newsletter</h4>
							<p>Souscrivez a notre newsletter pour suivre nos actualités!</p>
							<div class="form-group multiple-form-group input-group">
								<input type="email" name="email" class="form-control">
								<span class="input-group-btn">
                            <button type="button" class="btn btn-theme btn-add">Souscrire</button>
                        </span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="sub-footer">
				<div class="container">
					<div class="row">
						<div class="col-lg-6">
							<div class="copyright">
								<p>&copy; ISEV - Tous droits reservés</p>
								<div class="credits">
									<!--
                    All the links in the footer should remain intact. 
                    You can delete the links only if you purchased the pro version.
                    Licensing information: https://bootstrapmade.com/license/
                    Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Sailor
                  -->
									<address title="Adresse du developpeur">Par Cedric Nguendap, <a href="mailto:cednguendap@gmail.com">cednguendap@gmail.com</a> </address>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<ul class="social-network">
								<li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
								<li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<?php echo $this->url_for('js/jquery.min.js')?>"></script>
	<script src="<?php echo $this->url_for('js/modernizr.custom.js')?>"></script>
	<script src="<?php echo $this->url_for('js/jquery.easing.1.3.js')?>"></script>
	<script src="<?php echo $this->url_for('js/bootstrap.min.js')?>"></script>
	<script src="<?php echo $this->url_for('plugins/flexslider/jquery.flexslider-min.js')?>"></script>
	<script src="<?php echo $this->url_for('plugins/flexslider/flexslider.config.js')?>"></script>
	<script src="<?php echo $this->url_for('js/jquery.appear.js')?>"></script>
	<script src="<?php echo $this->url_for('js/stellar.js')?>"></script>
	<script src="<?php echo $this->url_for('js/classie.js')?>"></script>
	<script src="<?php echo $this->url_for('js/uisearch.js')?>"></script>
	<script src="<?php echo $this->url_for('js/jquery.cubeportfolio.min.js')?>"></script>
	<script src="<?php echo $this->url_for('js/google-code-prettify/prettify.js')?>"></script>
	<script src="<?php echo $this->url_for('js/animate.js')?>"></script>
	<script src="<?php echo $this->url_for('js/custom.js')?>"></script>
        <?php echo $this->footer(); ?>

</body>

</html>