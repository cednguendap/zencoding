<?php
namespace Applications\Frontend\Classes;


class FrontendApplication extends \Library\Classes\Application
{
	
	function __construct()
	{
		parent::__construct();
		$this->name='Frontend';
	}
	function run()
	{
            try 
            {
                $controller=$this->getControlleur();
		$controller->execute();
            } catch (Exception $ex) {
                $controller->page->addError($ex);
            }
            catch(\ErrorException $e)
            {
                $controller->page()->addErrorException($e);
            }
            finally 
            {
                $this->httpResponse->setPage($controller->page());
		$this->httpResponse->send();
            }
		
		
	}
}
?>