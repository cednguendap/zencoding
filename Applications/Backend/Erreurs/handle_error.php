<div class="x_content bs-example-popovers">
    <?php for($i=0;$i<count($listError);$i++){ 
        if($listError[$i]->getCode()==1 || $listError[$i]->getCode()==0) { ?>
            <div class="alert alert-warning alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <strong>Erreur Simple: </strong> <?php echo $listError[$i]->getMessage(); ?><br>
                Ligne: <?php echo $listError[$i]->getLine(); ?><br>
                Fichier: <?php echo $listError[$i]->getFile(); ?><br>
                Code: <?php echo $listError[$i]->getCode(); ?>
            </div>
    <?php } else {?>
            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <strong>Erreur Fatal: </strong> <?php echo $listError[$i]->getMessage(); ?><br>
                Ligne: <?php echo $listError[$i]->getLine(); ?><br>
                Fichier: <?php echo $listError[$i]->getFile(); ?><br>
                Code: <?php echo $listError[$i]->getCode(); ?>
            </div>           
    <?php } } ?>
</div>