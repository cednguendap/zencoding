<?php 
namespace Applications\Backend\Classes;


/**
* 
*/
class BackendApplication extends \Library\Classes\Application
{
	protected $personel;
	
	public function __construct()
	{
		parent::__construct();
		$this->name='Backend';
	}
	public function run()
	{
		//doit d'abord verfier si l'utilisateur est connecter
		if($this->user->isAuthentificated())
		{
            try 
            {
                $controller=$this->getControlleur();
                $controller->execute();
            } catch (Exception $ex) {
                $controller->page()->addError($ex);
            }
            catch(\ErrorException $e)
            {
                $controller->page()->addErrorException($e);
            }
            finally 
            {
                $this->httpResponse->setPage($controller->page());
                $this->httpResponse->send();
            }
		}
                
		
	}
}

?>